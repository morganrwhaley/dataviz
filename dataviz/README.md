## To Run ##
1. Ensure you have [node](https://nodejs.org) installed
2. Clone the project repo
3. Run `$ node server.js`
4. Navigate to http://localhost:8080/