## To Run ##
1. Ensure you have [node](https://nodejs.org) installed
2. Clone the project repo
3. Run `npm install express --save && npm install request --save`
4. Run `node server.js`
5. Navigate to http://localhost:8080/